package main

import (
	"encoding/base64"
	"encoding/binary"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"github.com/spaolacci/murmur3"
	"time"
)

var id uuid.UUID

func main() {
	for i := 0; i < 10; i++ {
		id, _ := uuid.NewV1()
		fmt.Println(id, " -> ", ToEncodedString(id.Bytes()), " -> ", ToEncodedString(UUIDHash(id.Bytes())))
		time.Sleep(10)
	}
}

func FromCanonicalUUID(uuidString string) ([]byte, error) {
	uuidVal, err := uuid.FromString(uuidString)
	if err != nil {
		return nil, err
	}
	return uuidVal.Bytes(), nil
}

func ToCanonicalUUID(data []byte) (string, error) {
	uuidVal, err := uuid.FromBytes(data)
	if err != nil {
		return "", err
	}

	return uuidVal.String(), nil
}

// Split prefix of incoming request and decrypt it from base64
func FromEncodedString(data string) (result []byte, err error) {
	result, err = base64.RawURLEncoding.DecodeString(data)
	return
}

func uintToByteSlice(num uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, num)
	return b
}

// Provides 8-byte hash string for incoming data
func UUIDHash(data []byte) []byte {
	return uintToByteSlice(murmur3.Sum64(data))
}

// Encode provided data byte array to base64 URL-safe format
func ToEncodedString(data []byte) string {
	return base64.RawURLEncoding.EncodeToString(data)
}
