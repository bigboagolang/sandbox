package main

import (
	"fmt"
	"net"
	"strconv"
	"sync"
)

func main() {
	//_ = pprof.StartCPUProfile(os.Stdout)
	//defer pprof.StopCPUProfile()

	//trace.Start(os.Stdout)
	//defer trace.Stop()

	var wg sync.WaitGroup
	pScan := 1024
	cpus := 100 //runtime.NumCPU()
	wg.Add(cpus)

	buff := make(chan int16, pScan)

	//buff <- int16(80)
	//buff <- int16(8080)
	//buff <- int16(443)
	//buff <- int16(8081)

	for i := 0; i < cpus; i++ {
		go worker(buff, &wg)
	}

	for i := 1; i <= pScan; i++ {
		buff <- int16(i)
	}
	close(buff)

	//for i := 0; i < cpus; i++ {
	//	go worker(buff, &wg)
	//}
	wg.Wait()
}

func worker(buff chan int16, wg *sync.WaitGroup) {
	res := ""
	//j := 1
	//for i:=1;i<10000;i++{
	//	j = j*i
	//}
	for p := range buff {

		s := fmt.Sprintf("scanme.nmap.org:%d", p)
		c, err := net.Dial("tcp", s)
		if err == nil {
			c.Close()
			res = res + " " + strconv.Itoa(int(p))
		}
	}
	if len(res) > 0 {
		//fmt.Printf("%v ports are open\n", res)
	}
	wg.Done()
}
