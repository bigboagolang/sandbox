package main

import (
	"context"
	"fmt"
)

func doWithCancel() {
	ctx := context.Background()
	ctxCancel, cancel := context.WithCancel(ctx)
	defer cancel()

	c := make(chan int, 3)

	go doRunA(ctxCancel, c)
	go doRunB(ctxCancel, c)
	go doRunC(ctxCancel, c)

	fmt.Println(<-c)
}

func doRunA(ctx context.Context, c chan int) {
	c <- 1
}
func doRunB(ctx context.Context, c chan int) {
	c <- 2
}
func doRunC(ctx context.Context, c chan int) {
	c <- 3
}
