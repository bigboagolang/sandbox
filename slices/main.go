package main

import "fmt"

type User struct {
	Name  string
	Email string
}

type Users []User

func main() {
	u := Users{
		{
			Name:  "name01",
			Email: "email01",
		},
		{
			Name:  "name02",
			Email: "email02",
		},
	}
	fmt.Println(u)

	for i, v := range u {
		v.Email = "updated email by v"  //working with a copy
		u[i].Name = "updated name by i" //working with slice
	}

	fmt.Println(u)
}
