package main

import "fmt"

type data struct {
	name string
	age  int
}

func (d data) displayName() {
	fmt.Println(d.name)
}

func (d *data) setAge(age int) {
	d.age = age
}

//https://learning.oreilly.com/videos/ultimate-go-programming/9780135261651/9780135261651-UGP2_01_04_01_03
func main() {
	d := data{
		name: "name01",
	}

	f1 := d.displayName //create a ref to func and copy of data, because displayName has variable semantic
	d.displayName()
	f1()
	d.name = "updated name"
	d.displayName()
	f1()

	f2 := d.setAge //create a ref to func and ref to data, because setName has ref semantic
	f2(25)
	fmt.Println(d)

}
